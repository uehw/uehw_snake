// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "math.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpawnAreaHeight = -20.0f;
	SpawnAreaLowerLeftPoint = FVector(-430, -430, 0);

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead) 
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			int RandElemNum = 1 + rand() % 3;
			Snake->AddSnakeElement(RandElemNum);
			this->SpawnNewFood();
			this->Destroy();
		}
	}
}

void AFood::SpawnNewFood()
{
	float RandX = SpawnAreaLowerLeftPoint.X + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (-1 * SpawnAreaLowerLeftPoint.X - SpawnAreaLowerLeftPoint.X)));
	float RandY = SpawnAreaLowerLeftPoint.Y + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (-1 * SpawnAreaLowerLeftPoint.Y - SpawnAreaLowerLeftPoint.Y)));

	FVector NewSpawnVector = FVector(RandX, RandY, SpawnAreaHeight);
	FTransform NewSpawnPoint(NewSpawnVector);
	AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewSpawnPoint);
}
